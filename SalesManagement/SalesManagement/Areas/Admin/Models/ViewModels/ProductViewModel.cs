﻿using Microsoft.AspNetCore.Mvc.Rendering;
using SalesManagement.Models;
using System.Collections.Generic;

namespace SalesManagement.Areas.Admin.Models.ViewModels
{
    public class ProductViewModel
    {
        public Product Product { get; set; }
        public List<SelectListItem> Categories  { get; set; }
    }
}
