USE [master]
GO
/****** Object:  Database [CSharp_LeKhanhThiDB]    Script Date: 6/20/2021 7:57:45 PM ******/
CREATE DATABASE [CSharp_LeKhanhThiDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'JV_SalesManagement', FILENAME = N'D:\Phanmem\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\JV_SalesManagement.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'JV_SalesManagement_log', FILENAME = N'D:\Phanmem\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\JV_SalesManagement_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CSharp_LeKhanhThiDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET  MULTI_USER 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [CSharp_LeKhanhThiDB]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 6/20/2021 7:57:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[Id] [int] NOT NULL,
	[Username] [varchar](250) NULL,
	[Password] [varchar](250) NULL,
	[FullName] [varchar](50) NULL,
	[Email] [varchar](250) NULL,
	[Status] [bit] NULL,
	[Address] [varchar](250) NULL,
	[Phone] [varchar](50) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Category]    Script Date: 6/20/2021 7:57:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[Status] [bit] NULL,
	[ParentId] [int] NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Invoice]    Script Date: 6/20/2021 7:57:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Invoice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[Created] [date] NULL,
	[Status] [int] NULL,
	[AccountId] [int] NULL,
 CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InvoiceDetails]    Script Date: 6/20/2021 7:57:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetails](
	[InvoiceId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Price] [money] NULL,
	[Quantity] [int] NULL,
 CONSTRAINT [PK_InvoiceDetails] PRIMARY KEY CLUSTERED 
(
	[InvoiceId] ASC,
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 6/20/2021 7:57:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[Description] [text] NULL,
	[Details] [text] NULL,
	[Status] [bit] NULL,
	[Price] [money] NULL,
	[Quantity] [int] NULL,
	[CategoryId] [int] NULL,
	[Featured] [bit] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 6/20/2021 7:57:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RoleAccount]    Script Date: 6/20/2021 7:57:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleAccount](
	[RoleId] [int] NOT NULL,
	[AccountId] [int] NOT NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_RoleAccount] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Account] ([Id], [Username], [Password], [FullName], [Email], [Status], [Address], [Phone]) VALUES (1, N'admin', N'$2b$10$h2DJtUPnMF5q.zqNrf/nTeQKfWusQtdd6D8Xf5xYhhvGh4wcxgKdq', N'Administrator', N'admin@gmail.com', 1, N'102 Dong Da, Da Nang', N'0900505050')
INSERT [dbo].[Account] ([Id], [Username], [Password], [FullName], [Email], [Status], [Address], [Phone]) VALUES (2, N'user2', N'$2b$10$aAZf3i5IXmgg17yzVk3n/O4XMT0ZP6Nx3rVl7WuMmJlQBrl50tapO', N'Doan Thi Thuy Linh', N'linh@gmail.com', 1, N'200 Hai Phong, Da Nang', N'0947664784')
INSERT [dbo].[Account] ([Id], [Username], [Password], [FullName], [Email], [Status], [Address], [Phone]) VALUES (3, N'user3', N'$2b$10$aAZf3i5IXmgg17yzVk3n/O4XMT0ZP6Nx3rVl7WuMmJlQBrl50tapO', N'Tran Quang Dang', N'dang@gmail.com', 1, N'255 Nguyen Van Linh,Da Nang', N'0845663723')
INSERT [dbo].[Account] ([Id], [Username], [Password], [FullName], [Email], [Status], [Address], [Phone]) VALUES (4, N'user4', N'$2b$10$h2DJtUPnMF5q.zqNrf/nTeQKfWusQtdd6D8Xf5xYhhvGh4wcxgKdq', N'Phan Hoang Nhu Anh', N'anh@gmail.com', 1, N'30 Bach Dang', N'0674877686')
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (1, N'Mobile Phone', 1, NULL)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (2, N'Laptop', 1, NULL)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (4, N'Iphone', 1, 1)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (5, N'SamSung', 1, 1)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (6, N'Redme', 1, 1)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (12, N'Tablet', 0, NULL)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (14, N'MacBook', 0, 2)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (16, N'Smart Watch', 1, NULL)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (18, N'HP', 1, 2)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (19, N'Dell', 1, 2)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (20, N'Ipad', 1, 12)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (21, N'SamSung', 1, 12)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (22, N'Apple Watch', 1, 16)
INSERT [dbo].[Category] ([Id], [Name], [Status], [ParentId]) VALUES (23, N'SamSung', 1, 16)
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Invoice] ON 

INSERT [dbo].[Invoice] ([Id], [Name], [Created], [Status], [AccountId]) VALUES (1, N'Invoice Online', CAST(N'2021-03-20' AS Date), 2, 3)
INSERT [dbo].[Invoice] ([Id], [Name], [Created], [Status], [AccountId]) VALUES (2, N'Invoice Online', CAST(N'2021-04-30' AS Date), 2, 2)
INSERT [dbo].[Invoice] ([Id], [Name], [Created], [Status], [AccountId]) VALUES (3, N'Invoice Online', CAST(N'2021-01-11' AS Date), 1, 2)
INSERT [dbo].[Invoice] ([Id], [Name], [Created], [Status], [AccountId]) VALUES (4, N'Invoice Online', CAST(N'2021-04-30' AS Date), 1, 2)
INSERT [dbo].[Invoice] ([Id], [Name], [Created], [Status], [AccountId]) VALUES (5, N'Invoice Online', CAST(N'2021-02-10' AS Date), 2, 3)
INSERT [dbo].[Invoice] ([Id], [Name], [Created], [Status], [AccountId]) VALUES (6, N'Invoice Online', CAST(N'2021-02-10' AS Date), 1, 4)
INSERT [dbo].[Invoice] ([Id], [Name], [Created], [Status], [AccountId]) VALUES (7, N'Invoice Online', CAST(N'2021-02-22' AS Date), 1, 4)
SET IDENTITY_INSERT [dbo].[Invoice] OFF
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (1, 1, 77.5000, 2)
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (1, 2, 92.9000, 1)
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (2, 3, 14.9900, 1)
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (3, 10, 49.9900, 2)
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (4, 14, 4.4990, 2)
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (5, 13, 2.5990, 1)
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (6, 5, 8.7990, 1)
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (6, 7, 98.9900, 3)
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (7, 4, 21.9990, 2)
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (7, 8, 117.9000, 1)
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (7, 9, 29.4900, 1)
INSERT [dbo].[InvoiceDetails] ([InvoiceId], [ProductId], [Price], [Quantity]) VALUES (7, 12, 38.8900, 1)
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (1, N'Apple iPhone 12 ', N'64GB-Purple ', N'6.1-inch (15.5 cm diagonal) Super Retina XDR display
Ceramic Shield, tougher than any smartphone glass
A14 Bionic chip, the fastest chip ever in a smartphone
Advanced dual-camera system with 12MP Ultra Wide and Wide cameras; Night mode, Deep Fusion, Smart HDR 3, 4K Dolby Vision HDR recording
12MP TrueDepth front camera with Night mode, 4K Dolby Vision HDR recording
Industry-leading IP68 water resistance
Supports MagSafe accessories for easy attachment and faster wireless charging
iOS with redesigned widgets on the Home screen, all-new App Library, App Clips and more', 1, 77.5000, 10, 4, 1)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (2, N'Apple iPhone 11 Pro Max ', N'256GB-Midnight Green', N'6.5-inch (16.5 cm diagonal) Super Retina XDR OLED display
Water and dust resistant (4 meters for up to 30 minutes, IP68)
Triple-camera system with 12MP Ultra Wide, Wide, and Telephoto cameras; Night mode, Portrait mode, and 4K video up to 60fps
12MP TrueDepth front camera with Portrait Mode, 4K video, and Slo-Mo
Face ID for secure authentication
A13 Bionic chip with third-generation Neural Engine
Fast charge with 18W adapter included', 1, 92.9000, 7, 4, 1)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (3, N'Samsung Galaxy M31', N'Ocean Blue, 6GB RAM, 128GB Storage', N'Quad Camera Setup - 64MP (F1.8) Main Camera +8MP (F2.2) Ultra Wide Camera +5MP(F2.2) Depth Camera +5MP(F2.4) Macro Camera and 32MP (F2.0) front facing Camera
6.4-inch(16.21 centimeters) Super Amoled - Infinity U Cut Display , FHD+ Resolution (2340 x 1080) , 404 ppi pixel density and 16M color support
Android v10.0 operating system with 2.3GHz + 1.7GHz Exynos 9611 Octa core processor , 6GB RAM, 128GB internal memory expandable up to 512GB and dual SIM
6000 mAh Battery
1 year manufacturer warranty for device and 6 months manufacturer warranty for in-box accessories including batteries from the date of purchase
Please contact Samsung helpline number 1800 407 267864 for any assistance related to device', 0, 15.0000, 15, 5, 1)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (4, N'Samsung Galaxy M42 5G ', N'Prism Dot Black, 6GB RAM, 128GB Storage', N'48MP+8MP+5MP+5MP Quad camera setup-48MP (F 1.8) main camera + 8MP (F2.2) Ultra wide camera+ 5MP (F2.4) depth camera + 5MP (2.4) Macro Camera| 20MP (F2.2) front camera
16.77 centimeters (6.6-inch) Super AMOLED - infinity U-cut display, HD+ resolution with 720 x 1600 pixels resolution, 265 PPI with 16M colours
Memory, Storage & SIM: 6GB RAM | 128GB internal memory expandable up to 1TB| SIM 1 + Hybrid (SIM or MicroSD) Nano Sim
OneUI 3.1 | Android 11 operating system with Qualcomm Snapdragon 750G octa core (2x2.2 GHz Cortex A77, 6x1.8 GHz Cortex A55) 5G processor, protected by Knox security
5000mAH lithium-ion battery, 1 year manufacturer warranty for device and 6 months manufacturer warranty for in-box accessories including batteries from the date of purchase', 1, 21.9990, 10, 5, 0)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (5, N'Redmi 9', N'Sky Blue, 4GB RAM, 64GB Storage', N'13+2MP Rear camera with AI Portrait, AI scene recognition, HDR, Pro mode | 5MP front facing camera
16.58 centimeters (6.53-inch) HD+ multi-touch capacitive touchscreen with 1600 x 720 pixels resolution, 268 ppi pixel density, 20:9 aspect ratio
Memory, Storage & SIM: 4GB RAM | 64GB storage expandable up to 512GB| Dual SIM with dual standby (4G+4G)
Android v10 operating system with 2.3GHz Mediatek Helio G35 octa core processor
5000mAH lithium-polymer battery with 10W wired charger in-box
1 year manufacturer warranty for device and 6 months manufacturer warranty for in-box accessories including batteries from the date of purchase
Box also includes: Power adapter, USB cable, SIM eject tool, Warranty card, User guide', 1, 8.7990, 15, 6, 0)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (6, N'Redmi Note 10', N'Shadow Black, 6GB RAM, 128GB Storage', N'Display: FHD+ (1080x2400) AMOLED Dot display; 16.33 centimeters (6.43 inch); 20:9 aspect ratio
Camera: 48 MP Quad Rear camera with 8MP Ultra-wide, 2MP Macro and Portrait lens| 13 MP Front camera
Battery: 5000 mAh large battery with 33W fast charger in-box and Type-C connectivity
Processor: Qualcomm Snapdragon 678 with Kryo 460 Octa-core; 11nm process; Up to 2.2GHz clock speed
Memory, Storage & SIM: 6GB RAM | 128GB UFS 2.2 storage expandable up to 512GB with dedicated SD card slot | Dual SIM (nano+nano) dual standby (4G+4G)', 1, 14.4990, 9, 6, 1)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (7, N'Apple MacBook Pro', N'13-inch, 8GB RAM, 256GB SSD, 1.4GHz Quad-core 8th-Generation Intel Core i5 Processor, Magic Keyboard', N'Eighth-generation quad-core Intel Core i5 processor
Brilliant Retina display with True Tone technology
Backlit Magic Keyboard
Touch Bar and Touch ID  
Intel Iris Plus Graphics 645
Ultra-fast SSD
Two Thunderbolt 3 (USB-C) ports', 0, 100.0000, 12, 14, 0)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (8, N'Apple MacBook Air with Apple M1 Chip', N' (13-inch, 8GB RAM, 512GB SSD) - Silver', N'Apple-designed M1 chip for a giant leap in CPU, GPU, and machine learning performance
Go longer than ever with up to 18 hours of battery life
8-core CPU delivers up to 3.5x faster performance to tackle projects faster than ever
Up to eight GPU cores with up to 5x faster graphics for graphics-intensive apps and games
16-core Neural Engine for advanced machine learning
8GB of unified memory so everything you do is fast and fluid
Superfast SSD storage launches apps and opens files in an instant', 1, 117.9000, 8, 14, 0)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (9, N'HP 14 (2021) Thin & Light Intel Celeron N4500 Laptop', N'Alexa Built-in, 8GB RAM, 256GB SSD, 14-inch HD Screen, Windows 10, MS Office', N'Processor: Intel Celeron N4500 (1.1 GHz base clock speed, up to 2.8 GHz with Intel Turbo Boost Technology, 4MB L3 cache, 2 cores)
Memory: 8 GB DDR4-2933 MHz RAM (1 x 8 GB), upgradable up to 16 GB (2 x 8 GB) | Storage: 256 GB PCIe NVMe M.2 SSD
Display: 14-Inch (35.6 cm) HD anti-glare micro-edge, 250 nits, 45% NTSC (1366 x 768)
Operating System & Software: Pre-loaded Windows 10 Home with lifetime validity | Pre-installed Microsoft Office Home & Student 2019, Alexa Built-in', 1, 29.4900, 20, 18, 1)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (10, N'HP 15(2021) 5th Gen Ryzen 5 5500U Laptop', N'Alexa Built-in, 8GB RAM, 512GB SSD, 15.6-Inch(39.6 cm) FHD Screen, Windows 10, MS Office', N'Processor: 5th Gen AMD Ryzen 5 5500U (2.1 GHz base clock speed, up to 4.0 GHz max boost clock, 8 MB L3 cache, 6 cores)
Memory: 8 GB DDR4-3200 SDRAM (1 x 8 GB), upgradable up to 16 GB (2 x 8 GB) | Storage: 512 GB PCIe NVMe M.2 SSD
Display: 15.6-Inch (39.6 cm) FHD SVA anti-glare micro-edge, 250 nits, 45% NTSC (1920 x 1080)
Operating System & Software: Pre-loaded Windows 10 Home with lifetime validity | Pre-installed Microsoft Office Home & Student 2019, Alexa Built-in
Graphics: AMD Radeon Graphics', 1, 49.9900, 12, 18, 1)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (11, N'Dell VOSTRO 3405 14inch (35.56 cms) HD Anti Glare Laptop', N'Ryzen-3 3250U / 4 GB / 1TB / Vega Graphics / 1 Yr NBD / Win 10 + MSO / Black/ 1.83Kg', N'Processor: :AMD Ryzen 3 3250U Mobile Processor with Radeon Graphics
Memory & Storage: 4GB DDR4, 2666MHz RAM |1TB 5400 rpm 2.5" SATA Hard Drivee
Display:14.0-inch FHD (1920 x 1080) Anti-glare LED Backlight Narrow Border WVA Display
Graphics: Integrated graphics with AMD APU AMD Radeon Graphics
Operating System & Software: Windows 10 Home | Microsoft Office Home and Student 2019
I/O ports: Two USB 3.2 Gen-1,One USB 2.0,One RJ45,One SD card slot ,One HDMI 1.4 port', 1, 34.5000, 15, 19, 1)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (12, N'Dell Inspiron 3501 15.6" (39.62 cms) FHD AG Display Laptop', N'i3-1005G1 / 8GB / 1TB / Integrated Graphics / Win 10 + MSO / Softmint', N'Processor:10th Generation Intel Core i3-1005G1 Processor (4MB Cache, up to 3.4 GHz)
Memory & Storage:8GB RAM | 1TB 5400 rpm 2.5" SATA Hard Drive
Display:15.6-inch FHD (1920 x 1080) Anti-glare LED Backlight Narrow Border WVA Display
Graphics: Intel UHD Graphics with shared graphics memory
Operating System & Software: Windows 10 Home Single Language | Microsoft Office Home and Student 2019
I/O ports: USB 3.2 Gen 1 (x2), USB2.0 (x1), HDMI 1.4(x1),One SD-card slot ,RJ45 - 10/100Mbps', 1, 38.8900, 7, 19, 1)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (13, N'Noise ColorFit Pro 2- India''s No. 1 Basic Smartwatch', N'24x7 Dynamic Heart Rate Tracking, 10 Day Battery, Full Touch HD Display & Multi-Sports Mode (Jet Black)', N'Bluetooth Version v5.0
Get a 1-year assured warranty from NOISE. Manage your warranty claims 24x7 by typing SUPPORT.GONOISE.COM in your browser''s address bar. For product related queries please reach out to us at 8882132132 or drop an email at PRODUCTFEEDBACK@NEXXBASE.COM
The brilliant 1.3" colour display is now full capacitive touch, supporting taps and swipes, so it is easy to read and operate.
The strong polycarbonate case makes the ColorFit Pro 2 featherlight on your wrist and is available in 4 beautiful colours with matching swappable straps.
24x7 heart rate monitoring with the built in optical HR monitor that measures your heart rate every five minutes.
9 sports modes to cover all your activities, whether you walk, run, hike, bike, treadmill, work-out, climb, spin, or perform yoga.
Log in to NoiseFit app and track your health and fitness details. Get insights on your progress and share your achievements with the world.', 1, 2.5990, 15, 16, 1)
INSERT [dbo].[Product] ([Id], [Name], [Description], [Details], [Status], [Price], [Quantity], [CategoryId], [Featured]) VALUES (14, N'Noise ColorFit NAV Smart Watch', N'Built-in GPS and High Resolution Display (Camo Green)', N'10 Sports Modes: Track your indoor and outdoor fitness activities and get detailed report on the progress made
Get a 1-year assured warranty from NOISE. Manage your warranty claims 24x7 by typing SUPPORT.GONOISE.COM in your browser''s address bar.
Full Touch: Swipe and tap on the display, with full touch controls
Smart notifications: For calls, texts and social media
Personalise your ColorFit NAV and choose from a plethora of cloud-based watch faces from the App', 1, 4.4990, 12, 16, 1)
SET IDENTITY_INSERT [dbo].[Product] OFF
INSERT [dbo].[Role] ([Id], [Name], [Status]) VALUES (1, N'Admin', 1)
INSERT [dbo].[Role] ([Id], [Name], [Status]) VALUES (2, N'Customer', 1)
INSERT [dbo].[RoleAccount] ([RoleId], [AccountId], [Status]) VALUES (1, 1, 1)
INSERT [dbo].[RoleAccount] ([RoleId], [AccountId], [Status]) VALUES (2, 2, 1)
INSERT [dbo].[RoleAccount] ([RoleId], [AccountId], [Status]) VALUES (2, 3, 1)
ALTER TABLE [dbo].[Category]  WITH CHECK ADD  CONSTRAINT [FK_Category_Category] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Category] ([Id])
GO
ALTER TABLE [dbo].[Category] CHECK CONSTRAINT [FK_Category_Category]
GO
ALTER TABLE [dbo].[Invoice]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Account] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[Invoice] CHECK CONSTRAINT [FK_Invoice_Account]
GO
ALTER TABLE [dbo].[InvoiceDetails]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceDetails_Invoice] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoice] ([Id])
GO
ALTER TABLE [dbo].[InvoiceDetails] CHECK CONSTRAINT [FK_InvoiceDetails_Invoice]
GO
ALTER TABLE [dbo].[InvoiceDetails]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceDetails_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[InvoiceDetails] CHECK CONSTRAINT [FK_InvoiceDetails_Product]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([Id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Category]
GO
ALTER TABLE [dbo].[RoleAccount]  WITH CHECK ADD  CONSTRAINT [FK_RoleAccount_Account] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[RoleAccount] CHECK CONSTRAINT [FK_RoleAccount_Account]
GO
ALTER TABLE [dbo].[RoleAccount]  WITH CHECK ADD  CONSTRAINT [FK_RoleAccount_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[RoleAccount] CHECK CONSTRAINT [FK_RoleAccount_Role]
GO
USE [master]
GO
ALTER DATABASE [CSharp_LeKhanhThiDB] SET  READ_WRITE 
GO
